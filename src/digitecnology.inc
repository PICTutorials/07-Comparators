;   Comparators - Play with the PIC comparators
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Comparators
;
;   Ths program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;======================================================
;Digitecnology General Function Module
;                        (Created by Steven Rodriguez)
;======================================================

;General description
;=============================
;This library provides some basic functions to do
;a variety of projects.
;=============================

;Assembler registers
;=============================
DELAY1 EQU 0x21
DELAY2 EQU 0x22
DELAY3 EQU 0x23
TIME1 EQU 0x24
TIME2 EQU 0x25
TIME3 EQU 0x26

;Functions
;=============================
chgbnk0 ; Changes to Bank 1 of the PIC
	bcf STATUS,RP1 ; Go to bank 0
        bcf STATUS,RP0
	return
chgbnk1 ; Changes to Bank 2 of the PIC
	bcf STATUS,RP1 ; Go to bank 1
        bsf STATUS,RP0
	return
chgbnk2 ; Changes to Bank 3 of the PIC
        bsf STATUS,RP1 ; Go to bank 1
        bcf STATUS,RP0
        return
chgbnk3 ; Changes to Bank 4 of the PIC
        bsf STATUS,RP1 ; Go to bank 1
        bsf STATUS,RP0
        return
delay1 ; DELAY1 = 249 = 999 uS delay ((DELAY1 - 1) * 3) + 4 + 2 + 2: uses 3 cycles (loop) + 4 cycles (call/return) + 2 cycle (last decfsz) + 2 (move DELAY to TIME)
	movf DELAY1,0 ; 1
	movwf TIME1 ; 1
	decfsz TIME1,1 ; 1/2
        goto $-1 ; 2
	return ; 4
delay2 ; DELAY1 = 249 = 999 uS + DELAY2 = 249 = (999 * DELAY2) + ((DELAY2 - 1) * 3) + 4 + 2 + 2 = 249501 us = 249.501 ms
	movf DELAY2,0 ; 1
        movwf TIME2 ; 1
	call delay1
	decfsz TIME2,1 ; 1/2
	goto $-2 ; 2
	return ; 4
delay3 ; DELAY1 = 249 = 999 uS + DELAY2 = 249 = 249.501 uS + DELAY3 = 249 = (249501 * DELAY3) + ((DELAY3 - 1) * 3) + 4 + 2 + 2 = 62126499 = 62.126499 s
	movf DELAY3,0 ; 1
        movwf TIME3 ; 1
	call delay2
        decfsz TIME3,1 ; 1/2
        goto $-2 ; 2
        return ; 4
