;   Comparators - Play with the PIC comparators
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Comparators
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Comparators (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the comparator module number 1
; - RB1 - 7 - bit 1 of the comparator module number 2
; - RA0 - 17 - Input of comparator module number 1 (2,5 V using 2 10K resistors)
; - RA1 - 18 - Input of comparator module number 2 (2,5 V using 2 10K resistors)
; - RA2 - 1 - Input of comparator module number 2 (0,7 V to 3,3 V using a 1K preset, a 100 ohm and 560 ohm resistor)
; - RA3 - 2 - Input of comparator module number 1 (0,9V using a 10K and 2K2 resistors)
;=============================

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start	

;Functions
;==============================
	include digitecnology.inc

;Program
;==============================
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	bcf TRISB,0 ;Output
	bcf TRISB,1 ;Output
	bsf TRISA,0 ;Input
	bsf TRISA,1 ;Input
	bsf TRISA,2 ;Input
	bsf TRISA,3 ;Input
	;Activate analog comparators
	call chgbnk0
	bcf CMCON,5 ;Comparators are not inverted
	bcf CMCON,4
	bsf CMCON,2 ;Two independent comparators
	bcf CMCON,1
	bcf CMCON,0	
Cycle
	;Show comparators value
	btfss CMCON,6
	goto $+3
	bsf PORTB,0
	goto $+2
	bcf PORTB,0
	btfss CMCON,7
        goto $+3
        bsf PORTB,1
        goto $+2
        bcf PORTB,1
	goto Cycle
	end
